/*
AntyCheat by Profesores
Gothic 2 Online
Last Edit: 2018-02-20

Hooks
*/

//set
_setPlayerHealth <- setPlayerHealth;
_setPlayerMaxHealth <- setPlayerMaxHealth;
//get
_getPlayerHealth <- getPlayerHealth;
_getPlayerMaxHealth <- getPlayerMaxHealth;

//others
_giveItem <- giveItem;
_removeItem <- removeItem;
_equipArmor <- equipArmor;
_equipMeleeWeapon <- equipMeleeWeapon;
_equipRangedWeapon <- equipRangedWeapon;